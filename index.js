'use strict'

const os = require("os")
const axios = require('axios').default
const crypto = require('crypto')
const express = require('express')
const querystring = require('querystring')
const formData = require('express-form-data')

const app = express()
const formDataOptions = {
  uploadDir: os.tmpdir(),
  autoClean: true
};
// parse data with connect-multiparty. 
app.use(formData.parse(formDataOptions))
// delete from the request all empty files (size == 0)
app.use(formData.format())
// union the body and the files
app.use(formData.union())
app.use(express.json())

const PORT = process.env.PORT || 3000
app.listen(PORT, () => console.log(`Listening on port ${PORT}!`))

app.get('/', async (req, res) => {
  try {
    console.log('========================Getting hello =========================')
    res.status(200).send('Hello, world!')
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting hello')
  }
})

app.get('/sms', async (req, res) => {
  try {
    console.log('========================Getting sms =========================')
    const data = {
      "code": 1234
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting sms')
  }
})

app.get('/requiredKycDocs', async (req, res) => {
  try {
    const country = req.query.country
    console.log('========================Getting kyc docs for country ' + country + ' =========================')
    let data = {}
    if (country == "India") {
      data = {
        'docs': ['Aadhaar', 'PAN']
      }
    } else if (country == "US") {
        data = {
          'docs': ['Address Proof Front', 'Address Proof Back']
        }
    } else {
        data = {
          'docs': ['Government ID Front', 'Government ID Back']
        }
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting kyc docs')
  }
})

app.get('/u/:user/home', async (req, res) => {
  try {
    console.log('========================Getting home =========================')
    const data = {
      numAccounts: 2,
      accounts: [{
          "name": "Crypto interest account",
          "balance": 1000,
          "currency": "USD",
          "id": "abc",
          "yield": "7%"
        },
        {
          "name": "USD savings account",
          "balance": 1000,
          "currency": "USD",
          "id": "abcd",
          "yield": "1%"
        }
      ]
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting home')
  }
})

app.get('/:user/profile', async (req, res) => {
  try {
    console.log('========================Getting profile =========================')
    const data = {
      "name": "John Doe",
      "email": "abc@gmail.com",
      "phone": "1234567890",
      "address": {
        "street1": "123 Main St",
        "city": "SF",
        "state": "CA",
        "postalCode": 94114
      }

    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting profile')
  }
})

app.get('/:user/loginActivity', async (req, res) => {
  try {
    console.log('========================Getting loginActivity =========================')
    const data = {
      "place": "SF, USA",
      "lastActive": "30 Sep 2020 12:00PM",
      "device": "Apple iphone",
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting loginActivity')
  }
})

app.get('/qr/:address', async (req, res) => {
  try {
    console.log('========================Getting qr code =========================')
    if (req.params.address != 'abc') {
      res.status(500).send()
    } else {
      res.status(200).sendFile(__dirname + '/testqr.png')
    }
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting qr code')
  }
})

app.get('/:account/home', async (req, res) => {
  try {
    console.log('========================Getting account home =========================')
    if (req.params.account != 'abc') {
      res.status(500).send()
    } else {
      const data = {
        "balance": 1000,
        "currency": "USD",
        "yield": "7%",
        "earned": 100
      }
      res.status(200).json(data)
    }
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting account home')
  }
})

app.get('/:account/detail', async (req, res) => {
  try {
    console.log('========================Getting account detail =========================')
    if (req.params.account != 'abc') {
      res.status(500).send()
    } else {
      const data = {
        numAccounts: 3,
        accounts: [{
            "name": "Ethereum",
            "balance": 1000,
            "currency": "ETH",
            "id": "abc",
            "yield": "7%",
            "priceUSD": 350
          },
          {
            "name": "USDC",
            "balance": 1000,
            "currency": "USDC",
            "id": "abc1",
            "yield": "7%",
            "priceUSD": 1
          }, {
            "name": "Dai",
            "balance": 1000,
            "currency": "DAI",
            "id": "abc2",
            "yield": "7%",
            "priceUSD": 1
          }
        ]
      }
      res.status(200).json(data)
    }
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting account detail')
  }
})

app.get('/:account/txns', async (req, res) => {
  try {
    console.log('========================Getting account txns =========================')
    if (req.params.account != 'abc') {
      res.status(500).send()
    } else {
      const data = {
        txns: [{
            "desc": "USD withdrawal",
            "type": "withdrawal",
            "currency": "USD",
            "amount": "100",
            "source": "abcd",
            "dest": "12abcfg",
            "timestamp": "July 30 2020 7:00 pm"
          },
          {
            "desc": "ETH deposit",
            "type": "deposit",
            "currency": "ETH",
            "amount": "100",
            "source": "abcde",
            "dest": "asafsf324",
            "timestamp": "July 20 2020 7:00 pm"
          },
          {
            "desc": "USDC transfer",
            "type": "transfer",
            "currency": "USDC",
            "amount": "100",
            "source": "abcd",
            "dest": "afwf3234",
            "timestamp": "July 10 2020 7:00 pm"
          }
        ]
      }
      res.status(200).json(data)
    }
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting account txns')
  }
})

app.get('/:account/payments', async (req, res) => {
  try {
    console.log('========================Getting account linked payments =========================')
    if (req.params.account != 'abc') {
      res.status(500).send()
    } else {
      const data = {
        linkedPayments: [{
            "name": "Chase account",
            "type": "Bank account",
            "id": "abc123"
          },
          {
            "name": "Citi account",
            "type": "Bank account",
            "id": "abc1234"
          }
        ]
      }
      res.status(200).json(data)
    }
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting account linked payments')
  }
})

app.get('/:user/notifications', async (req, res) => {
  try {
    console.log('========================Getting notifications =========================')
    const data = {
      notifications: {
        "id1" : {
          "id": "id1",
          "desc": "USD withdrawal from account abcde123",
          "title": "Withdraw",
          "timestamp": "July 30 2020 7:00 pm",
          "status": "new",
          "read": true,
          "action": "Test"
        },
        "id2": {
          "id": "id2",
          "desc": "USDC deposited to account abcde123",
          "title": "Deposit",
          "timestamp": "July 20 2020 7:00 pm",
          "status": "important",
          "read": false
        }, 
        "id3": {
          "id": "id23",
          "desc": "Your email is not verified. Please verify it immediately.",
          "title": "Email not verified",
          "timestamp": "July 30 2020 7:00 pm",
          "status": "action",
          "action": "Verify Email",
          "read": false
        }
      }
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while getting notifications')
  }
})

app.delete('/:account/payments/:payment', async (req, res) => {
  try {
    console.log('========================Deleting payment method =========================')
    const data = {}
    if (req.params.payment == 'test') {
      data.status = 'true'
    } else {
      data.status = 'false'
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while deleting payment method')
  }
})

app.post('/:user/notifications', async (req, res) => {
  try {
    console.log('========================Posting notifications and returning data =========================')
    console.log(req.body)
    const data = req.body
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while posting notifications')
  }
})

app.post('/sms', async (req, res) => {
  try {
    console.log('========================Verifying sms =========================')
    const data = {}
    if (req.body.code == 1234) {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while verifying sms')
  }
})

app.post('/:user/verifyEmail', async (req, res) => {
  try {
    console.log('========================Verifying email =========================')
    const data = {}
    if (req.body.email == 'abc@gmail.com') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while verifying email')
  }
})

app.post('/:account/deposit', async (req, res) => {
  try {
    console.log('========================Depositing into account =========================')
    console.log(req.body)
    const data = {}
    if (req.body.source == 'abc') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(req.body)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while depositing into account')
  }
})

app.post('/:account/withdraw', async (req, res) => {
  try {
    console.log('========================Withdrawing from account =========================')
    const data = {}
    if (req.body.dest == 'abc') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while withdrawing from account')
  }
})

app.post('/signup', async (req, res) => {
  try {
    console.log('========================Signing up =========================')
    const data = {}
    if (req.body.email == 'abc@gmail.com') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while signing up')
  }
})

app.post('/login', async (req, res) => {
  try {
    console.log('========================Logging in =========================')
    const data = {}
    if (req.body.email == 'abc@gmail.com') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while logging in')
  }
})

app.post('/logout', async (req, res) => {
  try {
    console.log('========================Logging out =========================')
    const data = {}
    data.status = true
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while logging out')
  }
})

app.post('/:user/resendEmail', async (req, res) => {
  try {
    console.log('========================Resending email =========================')
    const data = {}
    if (req.body.email == 'abc@gmail.com') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while resending email')
  }
})

app.post('/:user/forgotPassword', async (req, res) => {
  try {
    console.log('========================Sending password reset email =========================')
    const data = {}
    if (req.body.email == 'abc@gmail.com') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while sending password reset email')
  }
})

app.post('/:user/kyc', async (req, res) => {
  try {
    console.log('========================Storing kyc =========================')
    const data = {}
    console.log(req.body.idProof)
    if (req.body.name == 'abc') {
      data.status = true
    } else {
      data.status = false
    }
    res.status(200).json(data)
  } catch (error) {
    console.error(error)
    res.status(500).send('Error occured while storing kyc')
  }
})